import plotly.express as px
from plotly.subplots import make_subplots
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash import html
from dash.dependencies import Output,Input,State
import plotly.graph_objects as go

result = pd.read_excel('NewData.xlsx')
categorical_features = result.select_dtypes(include = ['object'])
numerical_features = result.select_dtypes(include = np.number)
from sklearn.impute import SimpleImputer
imputer = SimpleImputer(missing_values = np.nan, strategy ='most_frequent')
categorical_features[['RESULT']] = imputer.fit_transform(categorical_features[['RESULT']])

from sklearn.impute import SimpleImputer
imputer_num = SimpleImputer(missing_values = np.nan, strategy ='mean')
numerical_features[['ENG','DZO','PHY','CHE']] = imputer_num.fit_transform(numerical_features[['ENG','DZO','PHY','CHE']])

numerical_features.MATH.fillna(value=0,inplace=True)
numerical_features.BIO.fillna(value=0,inplace=True)

concat = [categorical_features, numerical_features]

df = pd.concat(concat, axis=1)

percentage_math_drop = ((df['ENG']+df['DZO']+df['PHY']+df['CHE']+df['BIO'])/500)*100
percentage_Bio_drop = ((df['ENG']+df['DZO']+df['PHY']+df['CHE']+df['MATH'])/500)*100
percentage_both = ((df['ENG']+df['DZO']+df['PHY']+df['CHE']+df['BIO']+df['MATH'])/600)*100

percentage_math_drop_df = df[df['MATH']==0]
percentage_math_drop_df['Percentage'] = percentage_math_drop
percentage_Bio_drop_df = df[df['BIO']==0]
percentage_Bio_drop_df['Percentage'] = percentage_Bio_drop

both_taken_df = df[df['BIO']!=0.0]
both_taken_df = both_taken_df[both_taken_df['MATH']!=0.0]
both_taken_df['Percentage'] = percentage_both

Exam_not_appared = df[df['RESULT']=='ABS']
Exam_not_appared['Percentage'] = 0.0

df = pd.concat([Exam_not_appared, percentage_math_drop_df, percentage_Bio_drop_df, both_taken_df])

abs_df = df[df['RESULT']=='ABS']

group_year_abs = abs_df.groupby('YEAR')
group_year_abs_df = group_year_abs.get_group(2012)

PCNA_df = df[df['RESULT']=='PCNA']
PCNA_df.head()

group_year_pcna = PCNA_df.groupby('YEAR')
group_year_pcna_df = group_year_pcna.get_group(2012)

PCA_df = df[df['RESULT']=='PCA']

group_year_pca = PCA_df.groupby('YEAR')
group_year_pca_df = group_year_pca.get_group(2012)

PCA_12 = PCA_df.groupby('YEAR')
PCA_12_df = PCA_12.get_group(2012)

PCA_std_12 = PCA_12_df['RESULT'] == 'PCA'
Total_PCA_std_12 = PCA_std_12.count()

PCNA_12 = PCNA_df.groupby('YEAR')
PCNA_12_df = PCNA_12.get_group(2012)

PCNA_std_12 = PCNA_12_df['RESULT'] == 'PCNA'
Total_PCNA_std_12 = PCNA_std_12.count()

ABS_12 = abs_df.groupby('YEAR')
ABS_12_df = ABS_12.get_group(2012)

ABS_std_12 = ABS_12_df['RESULT'] == 'ABS'
Total_ABS_std_12 = ABS_std_12.count()

### 2013
PCA_13 = PCA_df.groupby('YEAR')
PCA_13_df = PCA_13.get_group(2013)
PCA_std_13 = PCA_13_df['RESULT'] == 'PCA'
Total_PCA_std_13 = PCA_std_13.count()

PCNA_13 = PCNA_df.groupby('YEAR')
PCNA_13_df = PCNA_13.get_group(2013)
PCNA_std_13 = PCNA_13_df['RESULT'] == 'PCNA'
Total_PCNA_std_13 = PCNA_std_13.count()

ABS_13 = abs_df.groupby('YEAR')
ABS_13_df = ABS_13.get_group(2013)
ABS_std_13 = ABS_13_df['RESULT'] == 'ABS'
Total_ABS_std_13 = ABS_std_13.count()

### 2015
PCA_15 = PCA_df.groupby('YEAR')
PCA_15_df = PCA_15.get_group(2015)
PCA_std_15 = PCA_15_df['RESULT'] == 'PCA'
Total_PCA_std_15 = PCA_std_15.count()

PCNA_15 = PCNA_df.groupby('YEAR')
PCNA_15_df = PCNA_15.get_group(2015)
PCNA_std_15 = PCNA_15_df['RESULT'] == 'PCNA'
Total_PCNA_std_15 = PCNA_std_15.count()

ABS_15 = abs_df.groupby('YEAR')
ABS_15_df = ABS_15.get_group(2015)
ABS_std_15 = ABS_15_df['RESULT'] == 'ABS'
Total_ABS_std_15 = ABS_std_15.count()

### 2016
PCA_16 = PCA_df.groupby('YEAR')
PCA_16_df = PCA_16.get_group(2016)
PCA_std_16 = PCA_16_df['RESULT'] == 'PCA'
Total_PCA_std_16 = PCA_std_16.count()

PCNA_16 = PCNA_df.groupby('YEAR')
PCNA_16_df = PCNA_16.get_group(2016)
PCNA_std_16 = PCNA_16_df['RESULT'] == 'PCNA'
Total_PCNA_std_16 = PCNA_std_16.count()

ABS_16 = abs_df.groupby('YEAR')
ABS_16_df = ABS_16.get_group(2016)
ABS_std_16 = ABS_16_df['RESULT'] == 'ABS'
Total_ABS_std_16 = ABS_std_16.count()

### 2017
PCA_17 = PCA_df.groupby('YEAR')
PCA_17_df = PCA_17.get_group(2017)
PCA_std_17 = PCA_17_df['RESULT'] == 'PCA'
Total_PCA_std_17 = PCA_std_17.count()

PCNA_17 = PCNA_df.groupby('YEAR')
PCNA_17_df = PCNA_17.get_group(2017)
PCNA_std_17 = PCNA_17_df['RESULT'] == 'PCNA'
Total_PCNA_std_17 = PCNA_std_17.count()

ABS_17 = abs_df.groupby('YEAR')
ABS_17_df = ABS_17.get_group(2017)
ABS_std_17 = ABS_17_df['RESULT'] == 'ABS'
Total_ABS_std_17 = ABS_std_17.count()

### 2018
PCA_18 = PCA_df.groupby('YEAR')
PCA_18_df = PCA_18.get_group(2018)
PCA_std_18 = PCA_18_df['RESULT'] == 'PCA'
Total_PCA_std_18 = PCA_std_18.count()

PCNA_18 = PCNA_df.groupby('YEAR')
PCNA_18_df = PCNA_18.get_group(2018)
PCNA_std_18 = PCNA_18_df['RESULT'] == 'PCNA'
Total_PCNA_std_18 = PCNA_std_18.count()

ABS_18 = abs_df.groupby('YEAR')
ABS_18_df = ABS_18.get_group(2018)
ABS_std_18 = ABS_18_df['RESULT'] == 'ABS'
Total_ABS_std_18 = ABS_std_18.count()

### 2019
PCA_19 = PCA_df.groupby('YEAR')
PCA_19_df = PCA_19.get_group(2019)
PCA_std_19 = PCA_19_df['RESULT'] == 'PCA'
Total_PCA_std_19 = PCA_std_19.count()

PCNA_19 = PCNA_df.groupby('YEAR')
PCNA_19_df = PCNA_19.get_group(2019)
PCNA_std_19 = PCNA_19_df['RESULT'] == 'PCNA'
Total_PCNA_std_19 = PCNA_std_19.count()

ABS_19 = abs_df.groupby('YEAR')
ABS_19_df = ABS_19.get_group(2019)
ABS_std_19 = ABS_19_df['RESULT'] == 'ABS'
Total_ABS_std_19 = ABS_std_19.count()

df_20 = df.sort_values(by='Percentage', ascending=False)

group_year_2012 = df_20.groupby('YEAR')
group_year_2012_df = group_year_2012.get_group(2012)

group_year_2013 = df_20.groupby('YEAR')
group_year_2013_df = group_year_2013.get_group(2013)

group_year_2015 = df_20.groupby('YEAR')
group_year_2015_df = group_year_2015.get_group(2015)

group_year_2016 = df_20.groupby('YEAR')
group_year_2016_df = group_year_2016.get_group(2016)

group_year_2017 = df_20.groupby('YEAR')
group_year_2017_df = group_year_2017.get_group(2017)

group_year_2018 = df_20.groupby('YEAR')
group_year_2018_df = group_year_2018.get_group(2018)

group_year_2019 = df_20.groupby('YEAR')
group_year_2019_df = group_year_2019.get_group(2019)

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import plotly.graph_objs as go

app = dash.Dash(__name__, meta_tags=[{"name": "viewport", "content": "width=device-width"}])
server = app.server

tabs_styles = {
    'height': '44px',
    'align-items': 'center'
}
tab_style = {
    'borderBottom': '1px solid #d6d6d6',
    'padding': '6px',
    'fontWeight': 'bold',
    'border-radius': '15px',
    'background-color': '#F2F2F2',
    'box-shadow': '4px 4px 4px 4px lightgrey',

}

tab_selected_style = {
    'borderTop': '1px solid #d6d6d6',
    'borderBottom': '1px solid #d6d6d6',
    'backgroundColor': '#119DFF',
    'color': 'white',
    'padding': '6px',
    'border-radius': '15px',
}

app.layout = html.Div((

    html.Div([
        html.Div([
            html.Div([
                html.H3('Class 12 Result Data Analysis', style = {'margin-bottom': '0px', 'color': 'black'}),
            ])
        ], className = "create_container1 four columns", id = "title"),

    ], id = "header", className = "row flex-display", style = {"margin-bottom": "25px"}),

    html.Div([
        html.Div([
            dcc.Tabs(id = "tabs-styled-with-inline", value = 'tab-1', children = [
                dcc.Tab(label = 'Tab 1', value = 'tab-1', style = tab_style, selected_style = tab_selected_style),
                dcc.Tab(label = 'Tab 2', value = 'tab-2', style = tab_style, selected_style = tab_selected_style),
                dcc.Tab(label = 'Tab 3', value = 'tab-3', style = tab_style, selected_style = tab_selected_style),
                dcc.Tab(label = 'Tab 4', value = 'tab-4', style = tab_style, selected_style = tab_selected_style),
            ], style = tabs_styles),
            html.Div(id = 'tabs-content-inline')
        ], className = "create_container3 eight columns", ),
    ], className = "row flex-display"),

), id= "mainContainer", style={"display": "flex", "flex-direction": "column"})

@app.callback(Output('tabs-content-inline', 'children'),
              Input('tabs-styled-with-inline', 'value'))
def render_content(tab):
    if tab == 'tab-1':
        return html.Div([
            html.Div([
                html.Div([
                    html.P('Select Year', className = 'fix_label', style = {'color': 'black', 'margin-top': '2px'}),
                    dcc.Dropdown(id = 'select_years',
                                 multi = False,
                                 clearable = False,
                                 disabled = False,
                                 style = {'display': True},
                                 value = '2012',
                                 placeholder = 'Select Year',
                                 options = [{'label': '2012', 'value': '2012'},
                                            {'label': '2013','value': '2013'},
                                            {'label': '2015','value': '2015'},
                                            {'label': '2016','value': '2016'},
                                            {'label': '2017','value': '2017'},
                                            {'label': '2018','value': '2018'},
                                            {'label': '2019','value': '2019'}], className = 'dcc_compon'),

                ], className = "create_container2 six columns", style = {'margin-top': '20px'}),
            ], className = "row flex-display"),
            
            html.Div([
                html.Div([
                    dcc.Graph(id = 'top10_chart',
                              config = {'displayModeBar': 'hover'}),
                ], className = "create_container2 ten columns", style = {'margin-top': '10px'}),
            ], className = "row flex-display"),
        ])
    elif tab == 'tab-2':
        return html.Div([
            html.Div([
                html.Div([
                    html.P('Select Year', className = 'fix_label', style = {'color': 'black', 'margin-top': '2px'}),
                    dcc.Dropdown(id = 'select_year',
                                 multi = False,
                                 clearable = False,
                                 disabled = False,
                                 style = {'display': True},
                                 value = '2012',
                                 placeholder = 'Select Year',
                                 options = [{'label': '2012', 'value': '2012'},
                                            {'label': '2013','value': '2013'},
                                            {'label': '2015','value': '2015'},
                                            {'label': '2016','value': '2016'},
                                            {'label': '2017','value': '2017'},
                                            {'label': '2018','value': '2018'},
                                            {'label': '2019','value': '2019'}], className = 'dcc_compon'),

                ], className = "create_container2 six columns", style = {'margin-top': '20px'}),
            ], className = "row flex-display"),
            
            html.Div([
                html.Div([
                    dcc.Graph(id = 'result_chart',
                              config = {'displayModeBar': 'hover'}),
                ], className = "create_container2 ten columns", style = {'margin-top': '10px'}),
            ], className = "row flex-display"),
        ])
    elif tab == 'tab-3':
        return html.Div([
            html.H3('Display content here in tab 3', style = {'text-align': 'center', 'margin-top': '100px', 'color':'black'})
        ])
    
    elif tab == 'tab-4':
        return html.Div([
            html.H3('Display content here in tab 4', style = {'text-align': 'center', 'margin-top': '100px', 'color':'black'})
        ])

@app.callback(
Output('result_chart', 'figure'), 
[Input('select_year', 'value')]
)
#graph plot and styling
def update_graph(year):
    colors = ['green', 'orange', 'red']
    if year == '2012':
        return {'data':[go.Pie(labels = ['Total PCA', 'Total PCNA', 'Total ABS'],
                               values = [Total_PCA_std_12, Total_PCNA_std_12, Total_ABS_std_12],
                               marker = dict(colors = colors),
                               hoverinfo = 'label+value+percent',
                               textinfo = 'label+value',
                               textfont = dict(size = 11),
                               texttemplate = '%{label}: %{value} <br>(%{percent})',
                               textposition = 'auto',
                               hole = .7,
                               rotation = 100
                              ),
                       ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            hovermode = 'x',
                            title = {
                                'text': 'Total PCA, PCNA and ABS of: ' + (year),
                                'y': 0.9,
                                'x': 0.5,
                                'xanchor': 'center',
                                'yanchor': 'top'},
                            titlefont = {
                                'color': 'black',
                                'size': 15},
                            legend = {
                                'orientation': 'h',
                                'bgcolor': '#F2F2F2',
                                'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                            font = dict(
                                family = "sans-serif",
                                size = 12,
                                color = 'black')
                        ),                           
               }
    if year == '2013':
        return {'data':[go.Pie(labels = ['Total PCA', 'Total PCNA', 'Total ABS'],
                               values = [Total_PCA_std_13, Total_PCNA_std_13, Total_ABS_std_13],
                               marker = dict(colors = colors),
                               hoverinfo = 'label+value+percent',
                               textinfo = 'label+value',
                               textfont = dict(size = 11),
                               texttemplate = '%{label}: %{value} <br>(%{percent})',
                               textposition = 'auto',
                               hole = .7,
                               rotation = 100
                              ),
                       ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            hovermode = 'x',
                            title = {
                                'text': 'Total PCA, PCNA and ABS of: ' + (year),
                                'y': 0.9,
                                'x': 0.5,
                                'xanchor': 'center',
                                'yanchor': 'top'},
                            titlefont = {
                                'color': 'black',
                                'size': 15},
                            legend = {
                                'orientation': 'h',
                                'bgcolor': '#F2F2F2',
                                'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                            font = dict(
                                family = "sans-serif",
                                size = 12,
                                color = 'black')
                        ),                           
               }
    if year == '2015':
        return {'data':[go.Pie(labels = ['Total PCA', 'Total PCNA', 'Total ABS'],
                               values = [Total_PCA_std_15, Total_PCNA_std_15, Total_ABS_std_15],
                               marker = dict(colors = colors),
                               hoverinfo = 'label+value+percent',
                               textinfo = 'label+value',
                               textfont = dict(size = 11),
                               texttemplate = '%{label}: %{value} <br>(%{percent})',
                               textposition = 'auto',
                               hole = .7,
                               rotation = 100
                              ),
                       ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            hovermode = 'x',
                            title = {
                                'text': 'Total PCA, PCNA and ABS of: ' + (year),
                                'y': 0.9,
                                'x': 0.5,
                                'xanchor': 'center',
                                'yanchor': 'top'},
                            titlefont = {
                                'color': 'black',
                                'size': 15},
                            legend = {
                                'orientation': 'h',
                                'bgcolor': '#F2F2F2',
                                'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                            font = dict(
                                family = "sans-serif",
                                size = 12,
                                color = 'black')
                        ),                           
               }
    if year == '2016':
        return {'data':[go.Pie(labels = ['Total PCA', 'Total PCNA', 'Total ABS'],
                               values = [Total_PCA_std_16, Total_PCNA_std_16, Total_ABS_std_16],
                               marker = dict(colors = colors),
                               hoverinfo = 'label+value+percent',
                               textinfo = 'label+value',
                               textfont = dict(size = 11),
                               texttemplate = '%{label}: %{value} <br>(%{percent})',
                               textposition = 'auto',
                               hole = .7,
                               rotation = 100
                              ),
                       ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            hovermode = 'x',
                            title = {
                                'text': 'Total PCA, PCNA and ABS of: ' + (year),
                                'y': 0.9,
                                'x': 0.5,
                                'xanchor': 'center',
                                'yanchor': 'top'},
                            titlefont = {
                                'color': 'black',
                                'size': 15},
                            legend = {
                                'orientation': 'h',
                                'bgcolor': '#F2F2F2',
                                'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                            font = dict(
                                family = "sans-serif",
                                size = 12,
                                color = 'black')
                        ),                           
               }
    if year == '2017':
        return {'data':[go.Pie(labels = ['Total PCA', 'Total PCNA', 'Total ABS'],
                               values = [Total_PCA_std_17, Total_PCNA_std_17, Total_ABS_std_17],
                               marker = dict(colors = colors),
                               hoverinfo = 'label+value+percent',
                               textinfo = 'label+value',
                               textfont = dict(size = 11),
                               texttemplate = '%{label}: %{value} <br>(%{percent})',
                               textposition = 'auto',
                               hole = .7,
                               rotation = 100
                              ),
                       ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            hovermode = 'x',
                            title = {
                                'text': 'Total PCA, PCNA and ABS of: ' + (year),
                                'y': 0.9,
                                'x': 0.5,
                                'xanchor': 'center',
                                'yanchor': 'top'},
                            titlefont = {
                                'color': 'black',
                                'size': 15},
                            legend = {
                                'orientation': 'h',
                                'bgcolor': '#F2F2F2',
                                'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                            font = dict(
                                family = "sans-serif",
                                size = 12,
                                color = 'black')
                        ),                           
               }
    if year == '2018':
        return {'data':[go.Pie(labels = ['Total PCA', 'Total PCNA', 'Total ABS'],
                               values = [Total_PCA_std_18, Total_PCNA_std_18, Total_ABS_std_18],
                               marker = dict(colors = colors),
                               hoverinfo = 'label+value+percent',
                               textinfo = 'label+value',
                               textfont = dict(size = 11),
                               texttemplate = '%{label}: %{value} <br>(%{percent})',
                               textposition = 'auto',
                               hole = .7,
                               rotation = 100
                              ),
                       ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            hovermode = 'x',
                            title = {
                                'text': 'Total PCA, PCNA and ABS of: ' + (year),
                                'y': 0.9,
                                'x': 0.5,
                                'xanchor': 'center',
                                'yanchor': 'top'},
                            titlefont = {
                                'color': 'black',
                                'size': 15},
                            legend = {
                                'orientation': 'h',
                                'bgcolor': '#F2F2F2',
                                'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                            font = dict(
                                family = "sans-serif",
                                size = 12,
                                color = 'black')
                        ),                           
               }
    if year == '2019':
        return {'data':[go.Pie(labels = ['Total PCA', 'Total PCNA', 'Total ABS'],
                               values = [Total_PCA_std_19, Total_PCNA_std_19, Total_ABS_std_19],
                               marker = dict(colors = colors),
                               hoverinfo = 'label+value+percent',
                               textinfo = 'label+value',
                               textfont = dict(size = 11),
                               texttemplate = '%{label}: %{value} <br>(%{percent})',
                               textposition = 'auto',
                               hole = .7,
                               rotation = 100
                              ),
                       ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            hovermode = 'x',
                            title = {
                                'text': 'Total PCA, PCNA and ABS of: ' + (year),
                                'y': 0.9,
                                'x': 0.5,
                                'xanchor': 'center',
                                'yanchor': 'top'},
                            titlefont = {
                                'color': 'black',
                                'size': 15},
                            legend = {
                                'orientation': 'h',
                                'bgcolor': '#F2F2F2',
                                'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                            font = dict(
                                family = "sans-serif",
                                size = 12,
                                color = 'black')
                        ),                           
               }

@app.callback(
Output('top10_chart', 'figure'), 
[Input('select_years', 'value')]
)
#graph plot and styling
def update_graph(value):
    if value == '2012':
        x_2012 = group_year_2012_df[:10].NAME
        y_2012 = group_year_2012_df[:10].Percentage
        
        return {'data':[go.Bar(
                                x = y_2012,
                                y = x_2012,
                                orientation = 'h',
                                hoverinfo = 'text',
                                hovertext =
                                    '<b>Name</b>: ' + group_year_2012_df['NAME'].astype(str) + '<br>' +
                                    '<b>Percentage</b>: ' + group_year_2012_df['Percentage'].astype(str) + '<br>'
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Top 10 Students of: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>Name</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        )    
               }
        
    if value == '2013':
        x_2013 = group_year_2013_df[:10].NAME
        y_2013 = group_year_2013_df[:10].Percentage
        
        return {'data':[go.Bar(
                                x = y_2013,
                                y = x_2013,
                                orientation = 'h',
                                hoverinfo = 'text',
                                hovertext =
                                    '<b>Name</b>: ' + group_year_2013_df['NAME'].astype(str) + '<br>' +
                                    '<b>Percentage</b>: ' + group_year_2013_df['Percentage'].astype(str) + '<br>'
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Top 10 Students of: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>Name</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        )    
               }
        
    if value == '2015':
        x_2015 = group_year_2015_df[:10].NAME
        y_2015 = group_year_2015_df[:10].Percentage
        
        return {'data':[go.Bar(
                                x = y_2015,
                                y = x_2015,
                                orientation = 'h',
                                hoverinfo = 'text',
                                hovertext =
                                    '<b>Name</b>: ' + group_year_2015_df['NAME'].astype(str) + '<br>' +
                                    '<b>Percentage</b>: ' + group_year_2015_df['Percentage'].astype(str) + '<br>'
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Top 10 Students of: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>Name</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        )    
               }
    
    if value == '2016':
        x_2016 = group_year_2016_df[:10].NAME
        y_2016 = group_year_2016_df[:10].Percentage
        
        return {'data':[go.Bar(
                                x = y_2016,
                                y = x_2016,
                                orientation = 'h',
                                hoverinfo = 'text',
                                hovertext =
                                    '<b>Name</b>: ' + group_year_2016_df['NAME'].astype(str) + '<br>' +
                                    '<b>Percentage</b>: ' + group_year_2016_df['Percentage'].astype(str) + '<br>'
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Top 10 Students of: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>Name</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        )    
               }
    
    if value == '2017':
        x_2017 = group_year_2017_df[:10].NAME
        y_2017 = group_year_2017_df[:10].Percentage
        
        return {'data':[go.Bar(
                                x = y_2017,
                                y = x_2017,
                                orientation = 'h',
                                hoverinfo = 'text',
                                hovertext =
                                    '<b>Name</b>: ' + group_year_2017_df['NAME'].astype(str) + '<br>' +
                                    '<b>Percentage</b>: ' + group_year_2017_df['Percentage'].astype(str) + '<br>'
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Top 10 Students of: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>Name</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        )    
               }
    
    if value == '2018':
        x_2018 = group_year_2018_df[:10].NAME
        y_2018 = group_year_2018_df[:10].Percentage
        
        return {'data':[go.Bar(
                                x = y_2018,
                                y = x_2018,
                                orientation = 'h',
                                hoverinfo = 'text',
                                hovertext =
                                    '<b>Name</b>: ' + group_year_2018_df['NAME'].astype(str) + '<br>' +
                                    '<b>Percentage</b>: ' + group_year_2018_df['Percentage'].astype(str) + '<br>'
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Top 10 Students of: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>Name</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        )    
               }
    
    if value == '2019':
        x_2019 = group_year_2019_df[:10].NAME
        y_2019 = group_year_2019_df[:10].Percentage
        
        return {'data':[go.Bar(
                                x = y_2019,
                                y = x_2019,
                                orientation = 'h',
                                hoverinfo = 'text',
                                hovertext =
                                    '<b>Name</b>: ' + group_year_2012_df['NAME'].astype(str) + '<br>' +
                                    '<b>Percentage</b>: ' + group_year_2012_df['Percentage'].astype(str) + '<br>'
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Top 10 Students of: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>Name</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        )    
               }

if __name__ == '__main__':
    app.server.run(debug=True)

